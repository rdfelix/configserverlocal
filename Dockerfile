FROM openjdk:8u121-jre-alpine
ADD target/configserver-local-0.0.1-SNAPSHOT.jar configserver-local-0.0.1.jar
EXPOSE 8888
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar /configserver-local-0.0.1.jar"]

#PARA CRIAR A IMAGEM
#docker build -t configserver-local-image .

#PARA RODAR
#docker run -p 8888:8888 --name configserver-local-container configserver-local-image